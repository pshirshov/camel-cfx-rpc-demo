/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package demo.contractfirst;

import demo.contractfirst.codegen.*;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.List;


public class CustomerServiceImpl implements CustomerService {

    public List<Customer> getCustomersByName(String name) throws NoSuchCustomerException {
        if ("none".equals(name)) {
            NoSuchCustomer noSuchCustomer = new NoSuchCustomer();
            noSuchCustomer.setCustomerId(name);
            throw new NoSuchCustomerException("Didnt find customer", noSuchCustomer);
        }
        Customer cust = new Customer();
        cust.setName(name);
        cust.getAddress().add("");
        cust.setBirthDate(GregorianCalendar.getInstance().getTime());
        cust.setNumOrders(1);
        cust.setRevenue(10000);
        cust.setTest(new BigDecimal(1.5));
        cust.setType(CustomerType.BUSINESS);
        return Arrays.asList(new Customer[]{cust});
    }

}
