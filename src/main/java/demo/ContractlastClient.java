/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package demo;

import demo.contractlast.HelloWorld;
import demo.contractlast.User;
import demo.contractlast.UserImpl;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.soap.SOAPBinding;
import java.util.Map;

public final class ContractlastClient {

    private static final QName SERVICE_NAME 
        = new QName("http://contractlast.demo/", "HelloWorld");
    private static final QName PORT_NAME 
        = new QName("http://contractlast.demo/", "HelloWorldPort");


    private ContractlastClient() {
    } 

    public static void main(String args[]) throws Exception {
        Service service = Service.create(SERVICE_NAME);
        // Endpoint Address
        String endpointAddress = "http://localhost:9998/app/contractlast";//"local://hello";
        // If web service deployed on Tomcat deployment, endpoint should be changed to:
        // http://localhost:8080/java_first_jaxws-<cxf-version>/services/hello_world

        // Add a port to the Service
        //service.addPort(PORT_NAME, SOAPBinding.SOAP11HTTP_BINDING, endpointAddress);
        service.addPort(PORT_NAME, SOAPBinding.SOAP12HTTP_MTOM_BINDING, endpointAddress);
        
        HelloWorld hw = service.getPort(HelloWorld.class);
        System.out.println(hw.sayHi("World"));

        User user = new UserImpl("World");
        System.out.println(hw.sayHiToUser(user));

        //say hi to some more users to fill up the map a bit
        user = new UserImpl("Galaxy");
        System.out.println(hw.sayHiToUser(user));

        user = new UserImpl("Universe");
        System.out.println(hw.sayHiToUser(user));

        System.out.println();
        System.out.println("Users: ");
        Map<Integer, User> users = hw.getUsers();
        for (Map.Entry<Integer, User> e : users.entrySet()) {
            System.out.println("  " + e.getKey() + ": " + e.getValue().getName());
        }

        System.out.println("Sleep: "+hw.sleep(500));
        try {
        System.out.println("Exceptional: "+hw.exceptional());
        }catch (Throwable e) {
            System.out.println("Got exception");
            e.printStackTrace();
        }

    }

}
