/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package demo;


import demo.contractfirst.codegen.*;
import junit.framework.Assert;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

public class ContractfirstClient {

    public static void main(String args[]) throws Exception {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext(
                new String[]{"client-applicationContext.xml"});
        CustomerService customerService = (CustomerService) applicationContext
                .getBean("CustomerService");

        // First we test for the case where the exception should occur
        List<Customer> customers = null;
        // Now for the positive case were we expect to get back a list of 1 customer
        // If setting higher than 1 you can do a small load test
        for (int c = 0; c < 1; c++) {
            // Checking for german "Umlaut �" here
            customers = customerService.getCustomersByName("Mueller");
            Assert.assertEquals(1, customers.size());
            Assert.assertEquals("Mueller", customers.get(0).getName());
            System.out.println("Succesfully called service");
        }

        try {
            customers = customerService.getCustomersByName("none");
            Assert.fail("We should get an exception here");
        } catch (NoSuchCustomerException e) {
            System.out.println(e.getMessage() + " " + e.getFaultInfo().getCustomerId());
            System.out.println("This is an expected Exception");
        } catch (Exception e) {
            System.out.println(e + "This is an expected Exception");
        }

        System.out.println("finished");
        System.exit(0);
    }
}