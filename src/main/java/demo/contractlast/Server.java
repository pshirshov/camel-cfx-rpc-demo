/*
*
* This is CXF-only application, it's not required for camel demo
* */
package demo.contractlast;

//import org.apache.cxf.transport.local.LocalTransportFactory;

import javax.xml.namespace.QName;
import javax.xml.ws.Endpoint;
import javax.xml.ws.Service;
import javax.xml.ws.soap.SOAPBinding;
import java.util.concurrent.CountDownLatch;

public class Server {
    private static final QName SERVICE_NAME
            = new QName("http://server.contractlast.demo/", "HelloWorld");
    private static final QName PORT_NAME
            = new QName("http://server.contractlast.demo/", "HelloWorldPort");


    static class StringThread implements Runnable
    {
        private String str;
        final CountDownLatch latch;

        StringThread(String s, final CountDownLatch latch)
        {
            str = s;
            this.latch = latch;
        }

        public void run ( )
        {
            Service service = Service.create(SERVICE_NAME);
            // Endpoint Address
            String endpointAddress = "local://hello/hello";
            // If web service deployed on Tomcat deployment, endpoint should be changed to:
            // http://localhost:8080/java_first_jaxws-<cxf-version>/services/hello_world

            // Add a port to the Service
            service.addPort(PORT_NAME, SOAPBinding.SOAP11HTTP_BINDING, endpointAddress);

             System.out.println(service.getWSDLDocumentLocation());
            for (int x = 0; x < 100; ++x) {
                HelloWorld hw = service.getPort(HelloWorld.class);
                System.out.println(hw.sayHi(str));
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
            latch.countDown();
        }
    }

    public static void main(String args[]) throws Exception {
        HelloWorldImpl serverImpl = new HelloWorldImpl();
        Endpoint.publish("local://hello/hello", serverImpl);
        Endpoint.publish("http://localhost:9000/hello", serverImpl);
        System.out.println("Server ready...");

        final int maxcnt = 1;
        final CountDownLatch latch = new CountDownLatch(maxcnt);

        for (int x = 0; x < maxcnt; ++x) {
            StringThread t = new StringThread ("TRD"+x, latch);
            new Thread(t). start ( );
        }

        //org.apache.cxf.transport.local.LocalTransportFactory
        latch.await();
        System.out.println("Server exiting");
        System.exit(0);
    }
}

/* // wtf
ServerFactoryBean sf = new ServerFactoryBean();
sf.setAddress("local://hello");
sf.setServiceBean(new HelloWorldImpl());
sf.setServiceClass(HelloWorld.class); // Optionally specify the service interface
sf.create();
*/
/* // simple frontend, bad idea
ClientProxyFactoryBean cf = new ClientProxyFactoryBean();
cf.setAddress("local://hello");
cf.setServiceClass(HelloWorld.class); // Optionally specify the service interface
HelloWorld h = (HelloWorld) cf.create();
System.out.println(h.sayHi("lol"));
*/


/*Bus bus = BusFactory.getDefaultBus();
LocalTransportFactory localTransport = new LocalTransportFactory();
DestinationFactoryManager dfm = bus.getExtension(DestinationFactoryManager.class);
dfm.registerDestinationFactory("http://schemas.xmlsoap.org/soap/http", localTransport);
dfm.registerDestinationFactory("http://schemas.xmlsoap.org/wsdl/soap/http", localTransport);
dfm.registerDestinationFactory("http://cxf.apache.org/bindings/xformat", localTransport);
dfm.registerDestinationFactory("http://cxf.apache.org/transports/local", localTransport);

ConduitInitiatorManager extension = bus.getExtension(ConduitInitiatorManager.class);
extension.registerConduitInitiator("http://cxf.apache.org/transports/local", localTransport);
extension.registerConduitInitiator("http://schemas.xmlsoap.org/wsdl/soap/http", localTransport);
extension.registerConduitInitiator("http://schemas.xmlsoap.org/soap/http", localTransport);
extension.registerConduitInitiator("http://cxf.apache.org/bindings/xformat", localTransport);
*/

/*protected Server() throws Exception {
    // START SNIPPET: publish
    System.out.println("Starting Server");
    HelloWorldImpl implementor = new HelloWorldImpl();
    String address = "local://helloWorldhelloWorld";
    Endpoint.publish(address, implementor);
    // END SNIPPET: publish
}*/