package demo.contractlast;

/**
 * JAXB demo
 * http://cxf.apache.org/docs/jax-rs-data-bindings.html#JAX-RSDataBindings-JAXBsupport
 *
 * Date: 29.06.12
 * Time: 11:49
 */
//@XmlRootElement(name = "Customer")
public class Customer {
    private String name;
    private long id;

    public Customer() {
    }

    public void setName(String n) {
        name = n;
    }

    public String getName() {
        return name;
    }

    public void setId(long i) {
        id = i;
    }

    public long getId() {
        return id;
    }
}