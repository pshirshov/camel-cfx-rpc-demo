/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
// START SNIPPET: service
package demo.contractlast;

import org.codehaus.jackson.map.ObjectMapper;

import javax.jws.WebService;
import javax.ws.rs.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@WebService(endpointInterface = "demo.contractlast.HelloWorld",
            serviceName = "HelloWorld")
@Path("/")
@Produces("application/json")
//@BindingType(JSONBindingID.JSON_BINDING)
public class HelloWorldImpl implements HelloWorld {
    Map<Integer, User> users = new LinkedHashMap<Integer, User>();

    @GET
    @Path("/exc")
    public String exceptional() throws NullPointerException
    {
        throw new NullPointerException();
    }

    @GET
    @Path("/min")
    public String min() {
        return "hi";
    }

    @GET
    @Path("/list")
    public List<String> getlist() {
        List<String> ret = new ArrayList<String>();
        ret.add("xxx");
        ret.add("yyy");
        return ret;
    }

    @GET
    @Path("/list2")
    @Consumes("application/json")
    public List<String> getlist2(@QueryParam("text") String text) {
        List<String> ret = new ArrayList<String>();
        ret.add("xxx");
        ret.add("yyy");
        ret.add("text");
        return ret;
    }

    @GET
    @Path("/getcustomer/{customerId}/")
    public Customer getCustomer(@PathParam("customerId") String id) {
        Customer c = new Customer();
        c.setId(Integer.parseInt(id));
        return c;
    }


    @POST
    @Path("/changecustomer")
    public Customer changeCustomer(@FormParam("customer") String  cstr) throws IOException {
        // application/x-www-form-urlencoded is not required, form parsing is correct even with text/plain
        //
        // example form: customer=%7B%22name%22%3Anull%2C%22id%22%3A123%7D
        //
        // looks like that only strings are allowed for JAX-RS parameters
        // http://stackoverflow.com/questions/2697541/convert-json-query-parameters-to-objects-with-jax-rs
        System.err.println(cstr);
        Customer c = new ObjectMapper().readValue(cstr, Customer.class);
        c.setName("CHANGED!");
        return c;
    }


    @GET
    @Path("/get/{text}")
    public String sayHi(@PathParam("text") String text) {
        System.out.println("sayHi called");
        return "Hello " + text + "@"+Thread.currentThread().getName();
    }

    public String sleep(int duration) throws InterruptedException {
        Thread.sleep(duration);
        return "Sleeped for " + duration;
    }

    public String sayHiToUser(User user) {
        System.out.println("sayHiToUser called");
        users.put(users.size() + 1, user);
        return "Hello "  + user.getName();
    }

    public Map<Integer, User> getUsers() {
        System.out.println("getUsers called");
        return users;
    }

}
// END SNIPPET: service
