CXF Camel Example
-------------------

This example shows how to offer and consume services using JMS Transport of Apache Camel. The example uses camel
for JMS as the configuration in Camel is much easier than in CXF.
 
How to execute the example?

1) Prepare the project for eclipse
> mvn eclipse:eclipse

2) Download activemq
Download from http://activemq.apache.org/download.html

3) Start activemq with the default config
> bin/activemq.bat

3) Start CustomerServiceServer in eclipse by using run as on the class

4) Start CustomerServiceClient in eclipse the same way as above

The client should do two calls to the server. The first ends in an expected exception and the second
will succeed and return some data.

Additional information can be found at 
http://www.liquid-reality.de/
and
http://cwiki.apache.org/CXF20DOC/how-tos.html
